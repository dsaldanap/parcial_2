<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/RegionDAO.php';
/**
 * @author david
 *
 */
class Region{
    private $id;

    private $nombre;




    public function getId()
    {
        return $this->id;
    }
   
    public function getNombre()
    {
        return $this->nombre;
    }

  
    public function __construct($id="",$nombre=""){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->conexion= new Conexion();
        $this->regionDAO=new RegionDAO($this->id, $this->nombre);
        
    }

    
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->regionDAO->consultar());
        $registro= $this->conexion->extraer();
        $this->nombre=$registro[0];
        $this->conexion->cerrar();
       
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> regionDAO -> consultarTodos()); 
        $regiones = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $this->id=$registro[0];
            $this->nombre=$registro[1];
            $region = new Region($registro[0], $registro[1]); 
            array_push($regiones, $region);
        }
        $this -> conexion -> cerrar();
        return  $regiones;
    }
   
}
?>
