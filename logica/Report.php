<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/ReportDAO.php';

class Report{
    private $id;
    private $date;
    private  $new_cases;
    private $cumulative_cases;
    private  $new_death;
    private $cumulative_death;
    private  $id_country;

    public function __construct($id="", $date="",$new_cases="", $cumulative_cases="", $new_death="", $cumulative_death="", $id_country=""){
        $this->id=$id;
        $this->date=$date;
        $this->new_cases=$new_cases;
        $this->cumulative_cases=$cumulative_cases;
        $this->new_death=$new_death;
        $this->cumulative_death=$cumulative_death;
        $this->id_country=$id_country;
        $this->conexion= new Conexion();
        $this->reportesDAO=new ReportDAO($this->id,$this->date, $this->new_cases,$this->cumulative_cases,$this->new_death, $this->cumulative_death,$this->id_country);
    }
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->reportesDAO->consultar());
        $registro= $this->conexion->extraer();
        
        $this->date=$registro[0];
        $this->new_cases=$registro[1];
        $this->cumulative_cases=$registro[2];
        $this->new_death=$registro[3];
        $this->cumulative_death=$registro[4];
        $id_country= new Country($registro[5]);
            $id_country->consultar();
            $this -> conexion -> cerrar();
       
        
    }
    
    
    public function consultarTodos(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->reportesDAO->consultarTodos());
        
        $reportes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_country= new Country($registro[6]);
            $id_country->consultar();
            $reporte = new Report($registro[0], $registro[1], $registro[2], $registro[3],$registro[4], $registro[5],$id_country);
            array_push($reportes , $reporte );
        }
        $this -> conexion -> cerrar();
        return  $reportes ;
        
        
    }
    
}
?>