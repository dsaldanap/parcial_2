<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/CountryDAO.php';

class Country{
    private $id;
    private  $nombre;
    private  $id_region;
  

    public function getId()
    {
        return $this->id;
    }

 
    public function getNombre()
    {
        return $this->nombre;
    }

    public function getId_region()
    {
        return $this->id_region;
    }

    
   

    public function __construct($id="", $nombre="",  $id_region=""){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->id_region=$id_region;
        $this->conexion= new Conexion();
        $this->countryDAO=new CountryDAO($this->id,$this->nombre, $this->id_region);
        
    }
   
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->countryDAO->consultar());
        $registro= $this->conexion->extraer();
        
        $this->nombre=$registro[0];
        $id_region = new Region($registro[1]);
        $id_region -> consultar();
        $this -> id_region = $id_region;
        $this->conexion->cerrar();
    }
    
    
    public function consultarTodos(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->countryDAO->consultarTodos());
        
        $countrys = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_region= new Region($registro[2]);
            $id_region->consultar();
            $country = new Country($registro[0], $registro[1], $id_region);
            array_push($countrys, $country );
        }
        $this -> conexion -> cerrar();
        return  $countrys;
        
        
    }
    
 
        
    
    
   
}
?>