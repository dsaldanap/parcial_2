<?php


class ReportDAO{
    private $id;
    private $date;
    private  $new_cases;
    private $cumulative_cases;
    private  $new_death;
    private $cumulative_death;
    private  $id_country;

    public function __construct($id, $date,  $new_cases,$cumulative_cases, $new_death,  $cumulative_death,$id_country){
        $this->id=$id;
        $this->date=$date;
        $this->new_cases=$new_cases;
        $this->cumulative_cases=$cumulative_cases;
        $this->new_death=$new_death;
        $this->cumulative_death=$cumulative_death;
        $this->id_country=$id_country;
        
        
    }
   
    public function consultar(){
        
        return "select date, new_cases, cumulative_cases, new_deaths, cumulative_deaths, id_country_country FROM report WHERE 
        id_report = ' " . $this -> id . " ' ";
    }
    
    public function consultarTodos(){
        
        return "select id_report, date, new_cases, cumulative_cases, new_deaths, cumulative_deaths, id_country_country FROM report";
    }

   
   
}
?>